$(document).ready(function(){

	$('input[type="file"]').change(function() {
		
		let file = $(this)[0].files[0];
		let fileName = $(this)[0].files[0]['name'];
		let fileTempName = $(this)[0].files[0]['tmp_name'];
		let fileSize = $(this)[0].files[0]['size'];
		let error = $(this)[0].files[0]['error'];
		let type = $(this)[0].files[0]['type'];
		let allowedFormat = ['jpg', 'jpeg', 'png'];

		if($(this)[0].files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('.image-holder').find('img').attr('src', e.target.result);
			}
			reader.readAsDataURL($(this)[0].files[0]);
		}

		let fileExtn = fileName.split('.')[1].toLowerCase();

		if($.inArray(fileExtn, allowedFormat) >= 0) {
			if(fileSize <= 400000) {						
				let info = {file: file};
				console.log(info);
				$.ajax({
					url: 'uploadController.php',
					type: 'POST',
					data: new FormData(file),
					contentType: false,
					cache: false,
					processData: false,
					success: function(data) {
						console.log(data);
					}
				});

			} else {
				console.log('File is too big!');
			}

		} else {
			console.log('Incompatible File Format (jpeg, jpg, png).');
		}
		//fileName = fileName.split('.')[0] + Date.parse(new Date()) + '.' + fileExtn;
		//console.log('Extension: ' + fileExtn);
		//console.log(fileName);

	});
});