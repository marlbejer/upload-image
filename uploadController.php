<?php 

	//print_r($_POST['isFileName'])
	if(isset($_POST)) {

		$file = $_FILES['file'];
		$fileName = $file['name'];
		$fileTmpName = $file['tmp_name'];
		$fileSize = $file['size'];

		$fileExtension = explode('.', $fileName);
		$fileActualExtension = strtolower(end($fileExtension));

		$allowed_formats = array('jpg', 'jpeg', 'png');

		if(in_array($fileActualExtension, $allowed_formats)) {
			if($fileSize < 400000) {
				$fileNewName = $_POST['isFileName'].uniqid('', true).'.'.$fileActualExtension;
				$fileDestination = 'assets/'.$fileNewName;
				move_uploaded_file($fileTmpName, $fileDestination);
				echo 'File has been saved!';
			} else {
				echo 'File is too big!';
			}
		}
	}
 ?>